//Letras
PFont font1;
PFont font2;

//Imagenes
PImage img;

//Sonidos
import processing.sound.*;

SoundFile Alerta;
SoundFile Acelerar;
SoundFile Prender;
SoundFile Freno;

//Variables
float x = 100;
int wait = 1000;
int t,y = 0;

boolean init = true;

void setup(){
  size(530,650);
  smooth();
  Alerta = new SoundFile(this, "sonidos/amongus.mp3");
  Acelerar = new SoundFile(this, "sonidos/stutututu.mp3");
  Prender = new SoundFile(this, "sonidos/prender.mp3");
  Freno = new SoundFile(this, "sonidos/freno.mp3");
  Prender.play();
  img = loadImage("Stop_sign.png");
}

void draw(){
  background(191);
  
  font1 = loadFont("AdobeDevanagari-Regular-20.vlw");//Se carga la letra
  textFont(font1, 15);
  fill(0);
  
  fill(255);
  stroke(0);
  rect(50,70,410,210);
  
  //Tablero
  stroke(0);
  line(100,100,400,100);  //Límite superior
  line(100,250,400,250);  //Límite inferior
  line(100,100,100,150);  //0   km/h
  line(125,100,125,125);  //10  km/h
  line(150,100,150,150);  //20  km/h
  line(175,100,175,125);  //30  km/h
  line(200,100,200,150);  //40  km/h
  line(225,100,225,125);  //50  km/h
  line(250,100,250,150);  //60  km/h
  line(275,100,275,125);  //70  km/h
  line(300,100,300,150);  //80  km/h
  line(325,100,325,125);  //90  km/h
  line(350,100,350,150);  //100 km/h
  line(375,100,375,125);  //110  km/h
  line(400,100,400,150);  //120 km/h
  
  //Líneas de texto
  fill(0);
  text("km/h",235, 90);
  text("0", 97,165);
  text("20", 145,165);
  text("40", 195,165);
  text("60", 245,165);
  text("80", 295,165);
  text("100", 340,165);
  text("120", 390,165);
  
  //Luz de freno
  stroke(0);
  fill(255,255,255);
  ellipse(150,350,100,100);
  //Función Luz de freno:
  if ((mouseX > 100) && (mouseX < 200) && (x >= 99)){
    if ((mouseY > 500) && (mouseY < 600)){ 
      stroke(0);
      fill(255,0,0);
      ellipse(150,350,100,100);
      if(!Freno.isPlaying()){
        Freno.play(); 
      }
    }
  }
  
  //Cuadro de freno
  fill(255);
  text("Freno", 135, 495);
  rect(100,500,100,100);
  line(100,500,200,500);
  line(200,500,200,600);
  line(100,500,100,600);
  line(100,600,200,600);
  
  //Función frenado
  if (x >= 101){
    if ((mouseX > 100) && (mouseX < 200) && (mouseY > 500) && (mouseY < 600)){
      x--;
    }else{
    x = x - 0.16;
    Freno.stop();
    }
  }else{
    Freno.stop();
  }
  
  //Cuadro de aceleración
  fill(255);
  text("Acelerador", 350, 495);
  rect(330,500,100,100);
  line(330,500,430,500);
  line(430,500,430,600);
  line(330,500,330,600);
  line(330,600,430,600);
  
  font2 = loadFont("ErasITC-Light-48.vlw");//Se carga la letra
  textFont(font2, 48);
  
  //Función aceleración
    if ((mouseX > 350) && (mouseX < 450) && (mouseY > 500) && (mouseY < 600) && (x <= 399)){
      if (x >398.1){
        x=x-0.999;
      }
        if (y < 1){
          t = millis();
        }
        y++;
        if (millis() - t >= 10000){
          image(img, 150, 275, 250, 300);
          if (!Alerta.isPlaying()){
            Alerta.play();
          }
          if(Alerta.isPlaying()){
            Acelerar.stop();
          }
        }
        x++;
        if(!Acelerar.isPlaying()){
          Acelerar.play();
        }
        if(Acelerar.isPlaying()){
          Prender.stop();
        }
        fill(0);
        text((millis() - t)/1000, 368, 567);
    }else{
      y = 0;
      t = 0;
      Alerta.stop();
      Acelerar.stop();
    }

  //Indicador de velocidad
  stroke(255,0,0);
  strokeWeight(2);
  line(x,249,x,101);

}
